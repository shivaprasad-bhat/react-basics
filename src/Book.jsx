import React from 'react';

const Book = ({ img, title, author }) => {
    // Add attribute, eventHandler
    // onClick, onMouseOver etc
    const clickHandler = (e) => {
        alert('Hello World');
    };

    return (
        <article className="book">
            <img src={img} alt="Book Icon" />
            <h1 onClick={() => console.log(title)}>{title}</h1>
            <h4>{author}</h4>
            <button type="button" onClick={clickHandler}>
                View Details
            </button>
        </article>
    );
};

export default Book;
