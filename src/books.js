export const books = [
    {
        id: 1,
        img:
            'https://images-eu.ssl-images-amazon.com/images/I/71fgd%2BWAkHL._AC_UL200_SR200,200_.jpg',
        title: 'One Arranged Murder',
        author: 'Chetan Bhagat',
    },
    {
        id: 2,
        img:
            'https://images-eu.ssl-images-amazon.com/images/I/81s6DUyQCZL._AC_UL200_SR200,200_.jpg',
        title: 'Think Like a Monk',
        author: 'Jay Shetty',
    },
    {
        id: 3,
        img:
            'https://images-eu.ssl-images-amazon.com/images/I/91xAvyJmUUL._AC_UL200_SR200,200_.jpg',
        title: "Grandma's Bag of Stories",
        author: 'Sudha Murty',
    },
    {
        id: 4,
        img:
            'https://images-eu.ssl-images-amazon.com/images/I/91VokXkn8hL._AC_UL200_SR200,200_.jpg',
        title: 'Rich Dad Poor Dad',
        author: 'Robert Kiyosaki',
    },
];
